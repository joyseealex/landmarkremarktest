﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace landmarkremark.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new LandmarkContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<LandmarkContext>>()))
            {
                // Look for any movies.
                if (context.MapUsers.Any())
                {
                    return;   // DB has been seeded
                }

                context.MapUsers.AddRange(
                    new MapUsers
                    {
                        FirstName = "Joysee",
                        LastName = "Alex",
                        Email = "joysee@mail.com",
                        Password = "123qweasd"
                    },

                    new MapUsers
                    {
                        FirstName = "Mithun",
                        LastName = "Mathews",
                        Email = "mithun@mail.com",
                        Password = "123qweasd"
                    },

                    new MapUsers
                    {
                        FirstName = "Noah",
                        LastName = "Mathews",
                        Email = "noah@mail.com",
                        Password = "123qweasd"
                    }
                );

                context.SaveChanges();
               
            }
        }
    }
}
