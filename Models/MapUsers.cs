﻿using System;
using System.ComponentModel.DataAnnotations;

namespace landmarkremark.Models
{
    public class MapUsers
    {
        [Key]
        public int MapUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
