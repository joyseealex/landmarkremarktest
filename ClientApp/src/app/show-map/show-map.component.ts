import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { } from 'googlemaps';
//import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-counter-component',
  templateUrl: './show-map.component.html'
})
export class ShowMapComponent {
  @ViewChild('map') mapElement: any;
  map: google.maps.Map;
  //@ViewChild('content') contentPopUp: any;
  //closeResult: string;
  //constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
    const mapProperties = {
      center: new google.maps.LatLng(-33.836823, 151.207597),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,

    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);

    var infoWindow, map = this.map;
    infoWindow = new google.maps.InfoWindow;

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        infoWindow.setPosition(pos);
        var content = '<p>You are here.</p><p><button onclick"ViewNotes()">View Notes</button>'
        infoWindow.setContent(content);
        infoWindow.open(map);
        map.setCenter(pos);
      }, function () {
        this.handleLocationError(true, infoWindow, map.getCenter(), map);
      });
    } else {
      // Browser doesn't support Geolocation
      this.handleLocationError(false, infoWindow, map.getCenter(), map);
    }
  }

  handleLocationError(browserHasGeolocation, infoWindow, pos, map): void {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
      'Error: The Geolocation service failed.' :
      'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
  }

  ViewNotes(): void {
    //this.modalService.open(this.contentPopUp, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
    //  this.closeResult = `Closed with: ${result}`;
    //}, (reason) => {
    //  this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    //});
  }

  //private getDismissReason(reason: any): string {
  //  if (reason === ModalDismissReasons.ESC) {
  //    return 'by pressing ESC';
  //  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //    return 'by clicking on a backdrop';
  //  } else {
  //    return `with: ${reason}`;
  //  }
  //}

}

interface LocationNotes {
  LocationId: string;
  Lattitude: number;
  Longitude: number;
  Notes: string;
}
