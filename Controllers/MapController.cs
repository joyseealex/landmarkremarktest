﻿using landmarkremark.Models;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace landmarkremark.Controllers
{
    [Route("controller/[controller]")]
    public class MapController : Controller
    {
        private readonly LandmarkContext _context;

        public MapController(LandmarkContext context)
        {
            _context = context;
        }

        [HttpGet("[action]")]
        public void GetLocationNotes()
        {
            var users = _context.MapUsers;
        }


        [HttpPost("[action]")]
        public void CreateLocationNotes()
        {
            var users = _context.MapUsers;
        }

        public class LocationNotes
        {
            public int LocationId { get; set; }
            public long Lattitude { get; set; }
            public long Longitude { get; set; }

            public Notes[] locNotes { get; set; }
        }

        public class Notes
        {
            public int NoteId { get; set; }

            public string Message { get; set; }

            public int LocationId { get; set; }
        }
    }
}